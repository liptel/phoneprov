#!/usr/bin/env python
"""
Phoneprov

    @author: Jiri Slachta
    @version: 0.0.1
"""

import threading
import sys
import getopt
from monitors import astMon
from daemon import Daemon
import astutils

class Phoneprov(Daemon):
    def __init__(self, pidfile, args, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null',ast_path='/etc/asterisk/',ast_user_cfg='users.conf',outputdir='/var/www',server='127.0.0.1'):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile
        self.console_args = args
        self.ast_path = ast_path
        self.ast_user_cfg = ast_user_cfg
	self.outputdir = outputdir
	self.server=server

    def run(self):
	thread1 = astMon(self.ast_path, self.ast_user_cfg, self.outputdir, self.server)
        thread1.start()

if __name__ == "__main__":
	console_values = dict()

	# zero arg     - start with uci cfg
	# one arg      - stop, restart, help
	# more args    - start with console args

	if len(sys.argv) < 2:
		daemon = Phoneprov('/tmp/phoneprov.pid', ast_path='/etc/asterisk/', ast_user_cfg='users.conf', outputdir='/var/www', server_ip='127.0.0.1')
		print "Starting Phoneprov with default config"
		daemon.start()
		#daemon.run()
	elif len(sys.argv) == 2:
		if 'stop' == sys.argv[1]:
			daemon = Phoneprov('/tmp/phoneprov.pid', console_values)
			print "Stopping Phoneprov"
			daemon.stop()
		elif 'restart' == sys.argv[1]:
			daemon = Phoneprov('/tmp/phoneprov.pid', )
			print "Reloading Phoneprov"
			daemon.restart()
		elif 'nodaemon' == sys.argv[1]:
			astutils.create_prov_data(input_directory='/etc/asterisk', cfg='users.conf', output_directory='/var/www', server='127.0.0.1')
