#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import re,sys,os,argparse,socket
from xml.dom.minidom import *

class Peer:
	XML_TEMPLATE = """<?xml version="1.0" encoding="UTF-8" ?>
<gs_provision version="1">
        <mac>%(mac)s</mac>
        <config version="1">
                <P1802>%(server)s</P1802>
                <P1805>%(user)s</P1805>
                <P1806>%(password)s</P1806>
        </config>
</gs_provision>"""
	def __init__(self, serverip, outputdir):
		self.username = None
		self.password = None
		self.codecs = None
		self.macaddr = None
		self.serverip = serverip
		self.outputdir = outputdir

	def set_macaddr(self,macaddr):
		self.macaddr=macaddr.strip()

	def get_macaddr(self):
		return self.macaddr

	def set_user(self,user):
		self.user=user.strip()

	def get_user(self):
		return self.user

	def set_password(self,password):
		self.password=password.strip()

	def get_password(self):
		return self.password

	def set_codec(self,codec):
		if self.codecs is not None:
			self.codecs = ",".join((self.codecs,codec))
		else:
			self.codecs = codec

	def write(self):
		if self.macaddr is not None:
			xmlTemplate = self.XML_TEMPLATE

			data = {'mac':str(self.macaddr), 'server':str(self.serverip), 'user':str(self.user), 'password':str(self.password), 'codecs':str(self.codecs)}
			print "Creating provisioning data "+str(self.outputdir)+"/cfg"+str(self.macaddr)+".xml"
			text_file = open(str(self.outputdir)+"/cfg"+str(self.macaddr)+".xml", "w")
			text_file.write(xmlTemplate%data)
			text_file.close()
		else:
			print "No MAC address for peer "+str(self.username)
			print "No output provisioning file created."

def parse_config(users,output,serverip):
	file=open(users,"r")
	prog = re.compile('\[[a-zA-Z0-9]*\]')
	p = None
	for line in file:
		if prog.match(line.strip()):
			if p is not None:
				p.write()
				p = Peer(serverip,output)
			else:
				p = Peer(serverip,output)
		else:
			if p is not None:
				if '=' in line:
					key,value = line.strip().split('=')
					if key.strip() == "username":
						p.set_user(value.strip())
					elif key.strip() == "secret":
						p.set_password(value.strip())
					elif key.strip() == "allow":
						p.set_codec(value.strip())
					elif key.strip() == "macaddress":
						p.set_macaddr(value.strip())
	if p is not None: p.write()

	file.close()

def create_prov_data(input_directory='/etc/asterisk',cfg='users.conf',output_directory='/var/www',server='127.0.0.1'):
	input_file=input_directory+'/'+cfg

	if (os.access(output_directory, os.W_OK | os.X_OK)):
		if server is not None:
			if os.path.isfile(input_file):
				parse_config(input_file,output_directory,server)
			else:
				print input_file
				print "problem with input file"
		else:
			print "server variable is empty"
	else:
			print "cannot write to destination folder."
