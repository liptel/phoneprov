#!/usr/bin/env python
# -*- coding: utf-8 -*-

import threading
import os
import struct
import sys
import datetime
import errno
from inotifyx import *
from xml.dom.minidom import *
import re,sys,os,argparse,socket
import astutils

class astMon(threading.Thread):

	def __init__(self,ast_path='/etc/asterisk/',ast_user_cfg='users.conf',output_dir='/var/www',server_ip='127.0.0.1'):
		print 'initializing astMon'
	        threading.Thread.__init__(self)
		self.fs_flag='IN_CLOSE_WRITE'
		self.path=ast_path
		self.cfg=ast_user_cfg
		self.output_directory=output_dir
		self.server=server_ip

	def check_mask(self,event_type, mask):
		events = event_type.split("|")

		for event in events:
			if event in mask:
				return True
			return False

	def run(self):
		fd = init()

		try:
			wd = add_watch(fd, self.path)
			try:
				while True:
					events = get_events(fd)

					for event in events:
					# extract event name and fs event type from event
						filename = event.name
						event_type = event.get_mask_description()

						if (filename in self.cfg and self.fs_flag in event_type):
							astutils.create_prov_data(input_directory='/etc/asterisk',cfg='users.conf',output_directory='/var/www',server='127.0.0.1')
			except KeyboardInterrupt:
				pass
		finally:
			os.close(fd)
